package lab.facci_ojeda_velasco_jorge.lab4camara4a;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {



    Button buttonCamera, buttonGuardar;
    ImageView imageViewFoto;
    Bitmap imageBitmap;
    static final int REQUEST_IMAGE_CAPTURE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            buttonCamera = (Button) findViewById(R.id.buttonCamara);
            buttonGuardar = (Button) findViewById(R.id.buttonGuardar);
            imageViewFoto = (ImageView) findViewById(R.id.imageViewFoto);

            buttonCamera.setOnClickListener(this);

            buttonGuardar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GuardarImagen();
                }
            });

        }
        private  static  final int PERMISSION_SAVE = 101;
        private void GuardarImagen (){
        // preguntar si tenemos permiso a la SDCard
            if (tienePermisoSDCard()){
              //crear carpeta
                //guardar el archivo
                CrearCarpeta();

            }else{
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},PERMISSION_SAVE);
            }
        }

        private boolean tienePermisoSDCard(){
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        }

        private void CrearCarpeta(){
            String nombreCarpeta= Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+"Imagenes4A";
            File carpeta = new File(nombreCarpeta);
            if (!carpeta.exists()){
                if (carpeta.mkdir()) {
                    Toast.makeText(this, "Carpeta creada", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(this, "Carpeta no fue creada",Toast.LENGTH_LONG).show();
                }
            }
        }

    @Override
    public void onClick(View v) {
        //TODO: Llamar a la camara

        Intent tomarFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (tomarFoto.resolveActivity(getPackageManager()) != null){

        }
        {
            startActivityForResult(tomarFoto, REQUEST_IMAGE_CAPTURE);
        }

}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            Bundle extra = data.getExtras();
            imageBitmap = (Bitmap) extra.get("data");
            imageViewFoto.setImageBitmap(imageBitmap);
        }
    }
}
